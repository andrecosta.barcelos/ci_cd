#!/bin/bash
#$1 Secret File
if [ -f "$1" ]
    then
    while IFS= read -r sec
    do
        docker secret ls --format {{.Name}} | while read LINE
        do
            if [[ $LINE = $(echo $sec | cut -d " " -f 1) ]]; then
                docker secret rm $LINE
                break
            fi
        done
        echo "$(echo "$sec" | cut -d " " -f 2)" | tr -d '\n' | docker secret create $(echo "$sec" | cut -d " " -f 1) -
    done < "$1"
fi