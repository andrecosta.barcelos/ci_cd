#!/bin/bash
#$1 Config File
#$2 CI Project Name

if [ -f "$1" ]
    then
    docker config ls --format {{.Name}} | while read LINE
    do
        if [[ $LINE = $2-config ]]; then
            docker config rm $2-config
            break
        fi
    done
    docker config create $2-config .env
    unset configExits
fi